import { Command, Option, program } from 'commander';
import { parse } from "comment-json";
import * as fs from 'fs/promises';
import path from 'path';

/**
 * The type of a (single) value of a setting, may be used in an array if the setting is variadic.
 * The types folder and file actually are strings, but are used to generate help text.
 * Name is a string that must match the regular expression /^[a-zA-Z0-9_]+$/.
 * Int is a number that must be an integer.
 */
type SettingValueType = 'folder' | 'file' | 'int' | 'boolean' | 'string' | 'number' | 'name';

/**
 * A setting definition is used to create a {@link Setting}.
 */
export type SettingDefinition<T> = {
    /**
     * Name of the setting, used as long option in command line (e.g. --name)
     */
    name: string,
    /**
     * Short option in command line (e.g. -n)
     */
    short?: string,
    /**
     * Type of the setting, used to generate help text; if not given, the type of the default value is used to infer the type.
     */
    type?: SettingValueType,
    /**
     * Actual value may be embedded in settings file, e.g. as object or array.
     * This can only be defined for file settings. Instead of the filename, the actual value is written to the settings file.
     */
    embedded?: boolean,
    /**
     * Help text for the setting
     */
    help: string,
    /**
     * The default value of the setting
     */
    def: T,
    /**
     * If set to false, the setting is not loaded (or written) from a settings file.
     * This is useful for settings that are only used as command line arguments but using the same mechanism as settings.
     */
    settings?: boolean
}



async function folderExists(name: string) {
    try {
        const stat = await fs.stat(name)
        return stat.isDirectory();
    } catch (err) {
        return false;
    }
}
async function fileExists(name: string, hint?: string) {
    try {
        const stat = await fs.stat(name, {})
        return true;
    } catch (err) {
        return false;
    }
}

async function findAllSettingFiles(startDir: string, readOptions: ReadSettingsOptions) {
    const { settingsFolderName, settingsFileName, log, verb, warn, error, debug } = readOptions;
    const foundFiles = []

    startDir = path.resolve(startDir);
    let homeDir = process.env.HOME ? path.resolve(process.env.HOME) : null;
    if (homeDir && startDir.startsWith(homeDir)) {
        homeDir = null; // no special handling for home dir
    }
    let currentDir: string;
    let nextDir = startDir;
    do {
        currentDir = nextDir;
        const candidate = settingsFolderName ? path.join(currentDir, settingsFolderName, settingsFileName) : path.join(currentDir, settingsFileName);
        if (await fileExists(candidate)) {
            foundFiles.push(candidate);
            verb(`Found settings file ${candidate}`);
        } else {
            debug(`No settings file in ${candidate}`);
        }
        if (homeDir) { // home folder precedes other parent folders of start folder
            nextDir = homeDir;
            homeDir = null;
        } else {
            nextDir = path.dirname(currentDir);
        }
    } while (currentDir != nextDir);

    return foundFiles.reverse();
}


type SettingsType = {
    [key: string]: Setting;
}

type ConsoleFn = (message?: any, ...optionalParams: any[]) => void;
interface ConsoleOutputOptions {
    /**
     * Function to be called for log messages, defaults to console.log
     */
    log: ConsoleFn,
    /**
     * Function to be called for verbose messages, defaults to console.log
     */
    verb: ConsoleFn,
    /**
     * Function to be called for warn messages, defaults to console.warn
     */
    warn: ConsoleFn,
    /**
     * Function to be called for error messages, defaults to console.error
     */
    error: ConsoleFn
    /**
     * Function to be called for debug messages, defaults to console.debug
     */
    debug: ConsoleFn
}

interface ReadSettingsOptions extends ConsoleOutputOptions {
    /** 
     * The folder name where the settings file is searched, usually a hidden folder starting with a dot.
     * If not specified, the settings file is directly searched in the current folder.
     */
    settingsFolderName: string | null,
    /**
     * The name of the settings file, defaults to "settings.json"if not specified.
     */
    settingsFileName: string;
    /**
     * Optionally an explicitly defined settings file name, if not specified, the settings file is read from the settings file in the settings folder.
     * If specified, the settings file is read **only** from the explicitly defined file.
     */
    explicitSettingsFileName?: string;
}
interface WriteSettingsOptions extends ConsoleOutputOptions {
    /**
     * The program object, used for improving documentation of the settings, e.g. by adding the command names where the settings are used.
     * If not specified, the global program object is used. If not defined at all, no additional information is added.
     */
    program: Command,
    /** 
     * The folder name where the settings file is searched, usually a hidden folder starting with a dot.
     * If not specified, the settings file is directly searched in the current folder.
     */
    settingsFolderName: string,
    /**
     * The name of the settings file, defaults to "settings.json" if not specified.
     */
    settingsFileName: string;
    /**
     * Optionally an explicitly defined settings file name, if not specified, the settings file is written to the settings file in the settings folder.
     */
    explicitSettingsFileName?: string;
}



/**
 * Settings are used to configure the behavior of the CLI. 
 * The can be loaded from a settings file and used as {@link Option}s in Commander's {@link Command}s.
 * That enables user settings to be either stored in a file or passed as command line arguments.
 */
export class Setting<T = any> {

    /**
     * Reads the settings from possibly multiple settings file.
     * 
     * The settings files settingsFileName ("settings.json" by default) are searched in
     * - a subfolder settingsFolderName of the current folder
     * - and all its parent folders
     * - and in the home folder of the user (if it is parent folder already)
     * 
     * If a settings file is found, it is read and its content is merged with other setting files found in other folders.
     * The priority if the files is the order in which they are found, so settings in files found first precede
     * settings in files found later. I.e. settings in the current folder have the highest priority, then
     * settings in parent folders which are also ascendants of the home folder, then the home folder, and then parent folders which 
     * are not ascendants of the home folder.
     * 
     * Although the file format is JSON, comments are allowed (package comment-json is used for reading and writing).
     * 
     * @param settings the settings to be read, i.e. during reading the actual values of the settings are set.
     * @param readOptions options for reading the settings, e.g. the name of the settings file and the folder where the settings file is searched.
     */
    static async readSettings<T extends SettingsType>(settings: T, readOptions: Partial<ReadSettingsOptions> = {}) {
        const actReadOptions = {
            settingsFolderName: null, settingsFileName: "settings.json",
            log: console.log, verb: console.log, warn: console.warn, error: console.error, debug: console.debug,
            ...readOptions
        };
        const { log, verb, warn, error, debug } = actReadOptions;

        const settingsFiles: string[] = [];
        if (actReadOptions.explicitSettingsFileName) {
            settingsFiles.push(actReadOptions.explicitSettingsFileName);
        } else {
            settingsFiles.push(...await findAllSettingFiles(process.cwd(), actReadOptions));
        }

        for (const file of settingsFiles) {
            const settingsOfFile = await fs.readFile(file, 'utf8');
            const settingsObj: any = parse(settingsOfFile);
            for (const key of Object.keys(settingsObj)) {
                if (!(key in settings)) {
                    warn(`${file}: Unknown setting '${key}'`);
                    continue;
                }
                const value = settingsObj[key];
                const setting = settings[key];
                const typeError = setting.typeCheckErrorMsg(value);
                if (typeError) {
                    warn(`${file}: ${typeError}`);
                    continue;
                }
                setting.setActual(value);
            }
        }
    }

    /**
     * Writes the settings to the settings file settingsFileName, optionally in folder settingsFolderName, of the current working directory.
     * 
     * Although the file format is JSON, comments are used (package comment-json is used for reading and writing).
     * All settings (with settings flag enabled) are written to the file. 
     * Settings with actual value similar to default value are are uncommented, so that the setting file can be used as a template for user defined settings.
     * 
     * @param settings the settings to be written
     */
    static async writeSettings<T extends SettingsType>(settings: T, writeOptions: Partial<WriteSettingsOptions> = {}) {
        const actWriteOptions = {
            program: program, settingsFolderName: null, settingsFileName: "settings.json",
            log: console.log, verb: console.log, warn: console.warn, error: console.error, debug: console.debug,
            ...writeOptions
        };
        const { log, verb, warn, error, debug } = actWriteOptions;
        const settingsFile = 
            actWriteOptions.explicitSettingsFileName
            ? actWriteOptions.explicitSettingsFileName
            : actWriteOptions.settingsFolderName ? path.join(actWriteOptions.settingsFolderName, actWriteOptions.settingsFileName) : actWriteOptions.settingsFileName;

        if (await fileExists(settingsFile)) {
            warn(`Settings file ${settingsFile} already exists, not overwriting.`);
            return;
        }

        const settingsToCommands = new Map(Object.entries(settings).filter(([_, setting]) => setting instanceof Setting && setting.settings).map(([key, setting]) => [key, [] as string[]]));
        if (actWriteOptions.program) {
            program.commands.forEach(command => {
                Object.entries(command.opts()).forEach(([key, value]) => {
                    settingsToCommands.get(key)?.push(command.name());
                });
            });
        }

        let out = "{   /* Settings maybe overwritten by subfolder setting files or command line,\n"
            + "       overwrites values in parent settings files. */\n";

        settingsToCommands.forEach((commands, name) => {
            const setting = settings[name];
            const nameValue = `"${name}": ${JSON.stringify(setting.actual)}`;
            const space = nameValue.length < 25 ? " ".repeat(25 - nameValue.length) : "";
            out += `    ${setting.modified() ? "" : "// "}${nameValue}, ${space}/* ${setting.help} (${setting.variadic ? '...' : ''}${setting.type}, default: ${JSON.stringify(setting.def)})${commands.length > 0 ? `, used in ${commands.join(", ")}` : ""} */\n`;
        });

        out += "}\n";

        const folder = path.dirname(settingsFile);
        if (!await folderExists(folder)) {
            await fs.mkdir(folder, { recursive: true });
        }
        await fs.writeFile(settingsFile, out, 'utf8');

        log(`Settings written to ${settingsFile}`);
    }


    /**
     * The full name of the settings, used as long option in command line (e.g. --name)
     */
    public readonly name: string;
    /**
     * If defined, the short option in command line (e.g. -n)
     */
    private short: string | undefined;
    /**
     * The type of the setting, used to generate help text and validate input,
     * if undefined the type of the default value is used to infer the type.
     */
    public readonly type: SettingValueType | undefined;
    /**
     * If true, the actual value of the setting is embedded in the settings file, e.g. as object or array.
     */
    public readonly embedded: boolean;
    /**
     * The help text for the setting, added to settings file and shown in help text.
     */
    public readonly help: string;
    /**
     * The default value of the setting.
     */
    public readonly def: T;
    /**
     * If true, the setting is variadic, i.e. it can have multiple values and the actual value is an array of the type.
     */
    public readonly variadic: boolean;
    /**
     * The actual value of the setting, initially set to the default value.
     */
    public actual: T
    /**
     * If false, the setting is not loaded (or written) from (or to) a settings file.
     */
    public settings = true;

    constructor(spec: SettingDefinition<T>) {
        this.name = spec.name;
        this.short = spec.short;
        this.type = spec.type;
        this.embedded = !!spec.embedded;
        if (this.embedded && this.type !== 'file') {
            throw new Error(`Error defining setting ${this.name}: Only file settings can be embedded in settings file, was type ${this.type}.`)
        }
        if (spec.settings !== undefined) {
            this.settings = spec.settings;
        }

        if (!spec.type) {
            let val = spec.def instanceof Array ? spec.def[0] : spec.def;
            if (val === undefined) {
                val = "";
            }

            if (typeof val === 'number') {
                this.type = 'number';
            } else if (typeof val === 'boolean') {
                this.type = 'boolean';
            } else {
                this.type = 'string';
            }
        }
        this.variadic = spec.def instanceof Array;
        this.help = spec.help;
        this.def = spec.def;
        this.actual = this.def;
    }

    setActual(value: any) {
        this.actual = value;
    }

    modified() {
        if (typeof this.actual !== "object") {
            return this.actual !== this.def;
        }
        return JSON.stringify(this.actual) !== JSON.stringify(this.def);
    }

    /**
     * Returns null, if type of value conforms to the type of the setting, otherwise an error message.
     */
    typeCheckErrorMsg(value: any): string | null {

        const singleTypeConforms = (value: any): boolean => {
            switch (this.type) {
                case 'file':
                    if (this.embedded && typeof value === 'object') {
                        return true;
                    }
                // or else fall through
                case 'folder':
                case 'string':
                    return typeof value === 'string';
                case 'int':
                    return Number.isInteger(value);
                case 'boolean':
                    return typeof value === 'boolean';
                case 'number':
                    return typeof value === 'number';
                case 'name':
                    return /^[a-zA-Z0-9_]+$/.test(value);
            }
            throw new Error(`Unknown type ${this.type}`);
        }

        if (this.variadic) {
            if (!Array.isArray(value)) {
                return `Cannot set ${this.name} to ${JSON.stringify(value)} as it is not an array`;
            }
            for (const val of value) {
                if (!singleTypeConforms(val)) {
                    return `Cannot set item of ${this.name} to ${JSON.stringify(val)} as it is not a ${this.type}`;
                }
            }
        } else {
            if (!singleTypeConforms(value)) {
                return `Cannot set ${this.name} to ${JSON.stringify(value)} as it is not a ${this.type}`;
            }
        }
        return null;
    }

    /**
     * 
     * @returns an Option object that can be added to a Command object.
     */
    toOption() {
        let flag = "";
        if (this.short) {
            flag = `-${this.short}, `;
        }
        flag += `--${this.name}`;
        if (this.type !== 'boolean') {
            flag += ` <${this.type}`
            if (this.variadic) {
                flag += "...";
            }
            flag += ">";
        }


        let description = this.help;
        if (this.embedded) {
            if (!description.endsWith(".") && !description.endsWith("!")) {
                description += ".";
            }
            description += " Maybe defined directly in settings file.";
        }
        let defValueDesc = (this.actual !== this.def) ? `originally ${JSON.stringify(this.def)}, set to ${JSON.stringify(this.actual)}` : undefined;

        return new Option(flag, description).default(this.actual, defValueDesc);
    }

}