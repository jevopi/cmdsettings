import { mkdtempSync, readFileSync, rmSync } from "fs";
import path from "path";
import os from "os";
import { Setting } from "../src/settings";

let realCWD: string;
let tmpDir: string;
beforeEach(() => {
    realCWD = process.cwd();
    const tmpName = path.join(os.tmpdir(), "cmdsettings-");
    tmpDir = mkdtempSync(tmpName);
    process.chdir(tmpDir);
});

afterEach(() => {
    process.chdir(realCWD);
    rmSync(tmpDir, { recursive: true });
});


test("Write 2 settings", async () => {

    const settings = {
        opt1: new Setting({ name: "opt1", help: "help1", def: "def1", }),
        opt2: new Setting({ name: "opt2", help: "help2", def: "def2", })
    };

    await Setting.writeSettings(settings, { settingsFolderName: ".test" });

    const content = readFileSync(path.join(tmpDir, ".test", "settings.json"), "utf8");
    expect(content).toMatchInlineSnapshot(`
"{   /* Settings maybe overwritten by subfolder setting files or command line,
       overwrites values in parent settings files. */
    // "opt1": "def1",            /* help1 (string, default: "def1") */
    // "opt2": "def2",            /* help2 (string, default: "def2") */
}
"
`);
});

test("Write and read 2 settings", async () => {
    const settingsWritten = {
        opt1: new Setting({ name: "opt1", help: "help1", def: "def1", }),
        opt2: new Setting({ name: "opt2", help: "help2", def: "def2", })
    };
    settingsWritten.opt1.setActual("actVal1");
    await Setting.writeSettings(settingsWritten, { settingsFolderName: ".test" });

    const settingsRead = {
        opt1: new Setting({ name: "opt1", help: "help1", def: "def1", }),
        opt2: new Setting({ name: "opt2", help: "help2", def: "def2", })
    };
    await Setting.readSettings(settingsRead, { settingsFolderName: ".test" });

    expect(settingsRead.opt1.actual).toBe("actVal1");
});

test("Write and read settings with warnings", async () => {
    const settingsWritten = {
        opt1: new Setting({ name: "opt1", help: "help1", def: "def1", }),
        opt2: new Setting({ name: "opt2", help: "help2", def: "def2", })
    };
    settingsWritten.opt1.setActual("actVal1");
    settingsWritten.opt2.setActual("actVal2");
    await Setting.writeSettings(settingsWritten, { settingsFolderName: ".test" });

    const settingsRead = {
        other1: new Setting({ name: "other1", help: "help1", def: "def1", }),
        other2: new Setting({ name: "other2", help: "help2", def: "def2", })
    };
    const orgWarn = console.warn;
    const mockFn = jest.fn();
    console.warn = mockFn;
    try {
        await Setting.readSettings(settingsRead,{ settingsFolderName: ".test" });
    } finally {
        console.warn = orgWarn;
    }
    expect(mockFn.mock.calls.length).toBe(2);
});