import { Command, Help, Option, program } from "commander";
import { Setting, SettingDefinition } from "../src/settings"

test("Setting defaults", () => {
    const setting = new Setting({
        name: "theName",
        help: "theHelp",
        def: "theDef",
    })
    expect(setting.type).toBe("string");
    expect(setting.settings).toBe(true);
});


test.each([
    ["theDef", undefined, "sample", true],
    ["theDef", "file", "sample", true],
    ["theDef", "folder", "sample", true],
    [1, "int", "sample", false],
    [1, "int", 1, true],
    [false, "boolean", true, true],
    [1, "int", 1.5, false],
    [1, "number", 1.5, true],
    [["theDef"], undefined, "sample", false],
    [["theDef"], undefined, ["sample"], true],
    [["theDef"], undefined, [1], false],
    ["JohnDoe", "name", "DonaldDuck", true],
    ["JohnDoe", "name", "Donald Duck", false],
])("Setting.checkTypeOfValue def %s, type %s, value %s", (theDef: any, type: string | undefined, sample: any, matchesType: boolean) => {
    const spec: any = { name: "theName", help: "theHelp", def: theDef };
    if (type) {
        spec.type = type;
    }
    const setting = new Setting(spec);
    const checkResult = setting.typeCheckErrorMsg(sample);
    if (matchesType) {
        expect(checkResult).toBeNull();
    } else {
        expect(checkResult).toBeDefined();;
    }
});

test("Setting.checkTypeOfValue with embedded", () => {
    const spec: any = { name: "f", help: "file", def: "f", type: "file" };
    expect(new Setting(spec).typeCheckErrorMsg({})).toBeDefined();
    expect(new Setting({ ...spec, embedded: true }).typeCheckErrorMsg({})).toBeNull();
});

test.each<{ name: string, partSetting: Partial<SettingDefinition<any>>, expected: string }>([
    { name: "simple", partSetting: { def: "theDef" }, expected: '--theName <string> theHelp (default: "theDef")' },
    { name: "with short", partSetting: { def: "theDef", short: "x" }, expected: '-x, --theName <string> theHelp (default: \"theDef\")' },
    { name: "variadic", partSetting: { def: ["theDef"] }, expected: '--theName <string...> theHelp (default: ["theDef"])' },
    { name: "embedded", partSetting: { def: "theDef", type: "file", embedded: true }, expected: '--theName <file> theHelp. Maybe defined directly in settings file. (default: "theDef")' },
])("Setting.toOption $name", ({ name, partSetting, expected }) => {
    const h = new Help();
    const fullDescription = (option: Option) => h.optionTerm(option) + " " + h.optionDescription(option);
    const setting = new Setting({ name: "theName", help: "theHelp", ...partSetting } as SettingDefinition<any>);
    expect(fullDescription(setting.toOption())).toBe(expected);
});

test("Program with settings", () => {

    const programCommander = new Command();
    programCommander
        .option('-v, --verbose', 'Printout more', false)
        .option('--opt1 <string>', 'help1', 'def1')
        .option('--opt2 <file>', 'help2', 'def2');

    const settings = {
        verbose: new Setting({ short: "v", name: "verbose", help: "Printout more", def: false, settings: false }),
        opt1: new Setting({ name: "opt1", help: "help1", def: "def1", }),
        opt2: new Setting({ name: "opt2", type: "file", help: "help2", def: "def2", }),
    };

    // Example from the README
    
    const programSettings = new Command();
    programSettings
    .addOption(settings.verbose.toOption())
    .addOption(settings.opt1.toOption())
    .addOption(settings.opt2.toOption());
    
    expect(programCommander.opts()).toEqual(programSettings.opts());
    
    // Setting.readSettings(settings, { settingsFolderName: ".mytool" });
    
    // program.command("generateSettings").action(() => {
    //     Setting.writeSettings(settings, { settingsFolderName: ".mytool" });
    // });

    

    
});
